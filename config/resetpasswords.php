<?php

return [
    'ios_url_scheme' => env('IOS_URL_SCHEME',env('URL_SCHEME')),
    'android_url_scheme' => env('ANDROID_URL_SCHEME',env('URL_SCHEME')),
    'appname' => env('MOBILE_APP_NAME',env('APP_NAME'))
];
