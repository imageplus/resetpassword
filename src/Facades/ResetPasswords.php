<?php

namespace AppsPlus\ResetPasswords\Facades;

use Illuminate\Support\Facades\Facade;

class ResetPasswords extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'resetpasswords';
    }
}
