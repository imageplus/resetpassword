<?php

namespace AppsPlus\ResetPasswords;

use Illuminate\Support\ServiceProvider;

class ResetPasswordsServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'appsplus');
         $this->loadViewsFrom(__DIR__.'/../resources/views', 'appsplus');

        $this->app['router']->namespace('AppsPlus\\ResetPasswords\\Controllers')
            ->middleware(['web'])
            ->group(function () {
                $this->loadRoutesFrom(__DIR__ . '/Routes/web.php');
            });;
        $this->app['router']->namespace('AppsPlus\\ResetPasswords\\Controllers')
            ->middleware(['api'])
            ->group(function () {
                $this->loadRoutesFrom(__DIR__ . '/Routes/api.php');
            });;

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/resetpasswords.php', 'resetpasswords');

        // Register the service the package provides.
        $this->app->singleton('resetpasswords', function ($app) {
            return new ResetPasswords;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['resetpasswords'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/resetpasswords.php' => config_path('resetpasswords.php'),
        ], 'resetpasswords.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/appsplus'),
        ], 'resetpasswords.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/appsplus'),
        ], 'resetpasswords.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/appsplus'),
        ], 'resetpasswords.views');*/

        // Registering package commands.
        // $this->commands([]);
    }
}
