<?php

namespace AppsPlus\ResetPasswords\Controllers;



use AppsPlus\ResetPasswords\Requests\ResetPasswordRequest;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Mail\Message;
use Illuminate\Routing\Controller;
use AppsPlus\ResetPasswords\Requests\ChangePasswordRequest;
use Illuminate\Foundation\Auth\ResetsPasswords;
use AppsPlus\ResetPasswords\Traits\RespondsWithJson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Redirect;

class MobileResetPasswordController extends Controller
{
    use ResetsPasswords, RespondsWithJson, SendsPasswordResetEmails {
        ResetsPasswords::broker insteadof SendsPasswordResetEmails;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function credentials(Request $request)
    {
        return $request->merge(['password_confirmation' => $request->password])->only(
            'email', 'password', 'password_confirmation','token'
        );
    }

    public function change(ChangePasswordRequest $request)
    {
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
            $this->resetPassword($user, $password);
        });
        if ($response == "passwords.reset")
        {
            return $this->jsonResponse(true,'Reset Successfuly',null,200);
        }
        elseif ($response == "passwords.user")
        {
            return $this->jsonResponse(false,"The given data was invalid",["email" => [
                "The email must be a valid email address."
            ]],401);
        }
        elseif ($response == "passwords.token")
        {
            return $this->jsonResponse(false,"The given data was invalid",["token" => [
                "The given token was invalid."
            ]],401);
        }
        elseif ($response == "passwords.password")
        {
            return $this->jsonResponse(false,"The given data was invalid",["password" => [
                "The submitted password was incorrect."
            ]],401);
        }
        else{
            return $this->jsonResponse(false,"The request failed for an unknown reason",["unknown" => [
                "The request failed"
            ]],400);
        }
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function request(ResetPasswordRequest $request)
    {
        $credentials = ['email' => $request->email];
        $response = Password::sendResetLink($credentials);
        return $this->jsonResponse(true,'Password reset link attempt created. See data for details',$response,200);
    }


    public function redirect($token,$email,Request $request)
    {
        $user_agent = strtolower($request->userAgent());
        if(stripos($user_agent,"iphone") ||  stripos($user_agent,"ipad"))
        {
            return Redirect::to(config('resetpasswords.ios_url_scheme').'reset-password?token=' . $token . '&email=' . $email);
        }
        elseif(stripos($user_agent,"android"))
        {
            return Redirect::to(config('resetpasswords.android_url_scheme').'reset-password?token=' . $token . '&email=' . $email);
        }
        return Redirect::to('password/reset/' . $token);
    }
}
