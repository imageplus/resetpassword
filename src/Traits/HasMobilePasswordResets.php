<?php


namespace AppsPlus\ResetPasswords\Traits;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use AppsPlus\ResetPasswords\Notifications\MobileResetPasswordNotification;
use AppsPlus\ResetPasswords\Notifications\WebResetPasswordNotification;

trait HasMobilePasswordResets
{
    public function sendPasswordResetNotification($token)
    {
        $user_agent = strtolower(Request::userAgent());
        if(stripos($user_agent,"iphone") ||  stripos($user_agent,"ipad") || stripos($user_agent,"android"))
        {
            $this->notify(new MobileResetPasswordNotification($token, $this->email));
        }
        else{
            $this->notify(new WebResetPasswordNotification($token, $this->email));
        }

    }
}
