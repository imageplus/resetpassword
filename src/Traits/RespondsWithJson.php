<?php


namespace AppsPlus\ResetPasswords\Traits;


trait RespondsWithJson
{
    public function jsonResponse($success,$message,$data,$code)
    {
        return response()->json([
            "message" => $message,
            $success ? "data" : "errors" => [
                $data
            ]
        ], $code);
    }
}
